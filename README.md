# A New AODV Route Discovery Protocol to Achieve Fair Routing for Mobile Ad Hoc Networks

FP Topik Khusus

Kelompok 8 :
- Achmad Hanif Pradipta (05111640000154)
- Fawwaz Zuhdan Nauvali (05111740000106)
- Meila Kamilia (05111740000189)
- Ihdiannaja (05111740007005)

### Deskripsi Paper
Implementasi Developed AODV didasarkan pada paper berikut :

- Judul : A New AODV Route Discovery Protocol to Achieve Fair Routing for Mobile Ad Hoc Networks
- Penulis : Masaru Yoshimachi and Yoshifumi Manabe 
- Tahun Terbit : 2016
- Sumber : https://ieeexplore.ieee.org/document/7784247

### Tujuan Modifikasi
Mengurangi routing overhead dengan mencatat *fair value* dari *node neighbor*.

### Modifikasi
Modifikasi yang diusulkan pada protokol ini adalah:

1. Proses pertama sama seperti AODV
2. Pada proses propagasi RREQ, terdapat penghitungan FV pada header paket. Node yang menerima paket RREQ menambahkan informasi routing ke routing table. Kemudian membandingkan FV dan mengecek apakah node tersebut merupkan destination node. Jika bukan destination node dan belum menerima paket RREQ dengan RREQ ID yang sama, maka simpan FV yang terdapat pada RREQ di node. Kemudian simpan node ID dan FV dari node di paket RREQ. Jika node sudah menerima RREQ dengan ID yang sama, maka bandingkan FV dari RREQ yang baru diterima dan FV yang terdapat pada node. Jika FV RREQ yang baru diterima lebih kecil dibandingkan yang terdapat pada node, maka ganti FV yang telah tersimpan pada routing table. Jika tidak, drop paket RREQ.
3. FV ditambahkan pada paket RREP. RREP memiliki nilai dari semua nilai FV pada RREQ yang ditambahkan. Jika node yang menerima RREQ dalam node destination, maka cek FV dan FV yang tersimpan di node. Jika FV dari RREQ yang baru diterima lebih kecil dibandingkan yang sudah ada, maka node mengirim RREQ paket dan mengirimkannya ke source node.
4. Source node memilih rute yang pertama diterima dari RREP. Jika node menerima RREP baru yang memiliki FV yang lebih kecil dibandingkan rute sekarang, maka node memilih rute dari RREP yang baru diterima.


